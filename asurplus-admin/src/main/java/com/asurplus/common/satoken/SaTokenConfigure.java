package com.asurplus.common.satoken;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson.JSON;
import com.asurplus.common.consts.SystemConst;
import com.asurplus.common.enums.SaTokenExceptionEnum;
import com.asurplus.common.utils.RES;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Sa-Token代码方式进行配置
 *
 * @author asurplus
 */
@Configuration
public class SaTokenConfigure {

    /**
     * 全局过滤器
     *
     * @return
     */
    @Bean
    public SaServletFilter getSaServletFilter() {
        return new SaServletFilter()
                // 拦截路由
                .addInclude("/**")
                // 放行路由
                .addExclude(
                        "/kaptcha-image",
                        // 登录
                        "/login",
                        // 登出
                        "/logout",
                        // api接口
                        "/api/**",
                        // api文档
                        "/swagger-resources/**",
                        "/webjars/**",
                        "/v2/**",
                        "/swagger-ui.html/**",
                        "/doc.html/**",
                        "/error",
                        "/favicon.ico",
                        // 静态资源
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                )
                // 认证函数: 每次请求执行
                .setAuth(obj ->
                        SaRouter
                                // 需要验证的
                                .match("/**")
                                // 验证方法
                                .check(r -> StpUtil.checkLogin())
                )
                // 异常处理函数：每次认证函数发生异常时执行此函数
                .setError(this::returnError)
                // 前置函数：在每次认证函数之前执行
                .setBeforeAuth(r -> {
                    // ---------- 设置一些安全响应头 ----------
                    SaHolder.getResponse()
                            // 允许指定域访问跨域资源
                            .setHeader("Access-Control-Allow-Origin", "*")
                            .setHeader("Access-Control-Allow-Methods", "*")
                            .setHeader("Access-Control-Max-Age", "3600")
                            .setHeader("Access-Control-Allow-Headers", "*")
                            .setHeader("Content-Type", "application/json;charset=UTF-8")
                            // 服务器名称
                            .setServer(SystemConst.SYSTEM_ITEM_NAME)
                            // 是否可以在iframe显示视图： DENY=不可以 | SAMEORIGIN=同域下可以 | ALLOW-FROM uri=指定域名下可以
                            .setHeader("X-Frame-Options", "SAMEORIGIN")
                            // 是否启用浏览器默认XSS防护： 0=禁用 | 1=启用 | 1; mode=block 启用, 并在检查到XSS攻击时，停止渲染页面
                            .setHeader("X-XSS-Protection", "1; mode=block")
                            // 禁用浏览器内容嗅探
                            .setHeader("X-Content-Type-Options", "nosniff");
                    // 如果是预检请求，则立即返回到前端
                    SaRouter.match(SaHttpMethod.OPTIONS)
                            .free(obj -> {
                            })
                            .back();
                });
    }

    /**
     * 组装错误信息
     *
     * @param e
     * @return
     */
    public String returnError(Throwable e) {
        // 未登录
        SaTokenExceptionEnum resEnum = null;
        NotLoginException nle = (NotLoginException) e;
        switch (nle.getType()) {
            case NotLoginException.NOT_TOKEN:
                resEnum = SaTokenExceptionEnum.NO_TOKEN;
                break;
            case NotLoginException.INVALID_TOKEN:
                resEnum = SaTokenExceptionEnum.ERROR_TOKEN;
                break;
            case NotLoginException.TOKEN_TIMEOUT:
                resEnum = SaTokenExceptionEnum.EXPIRE_TOKEN;
                break;
            case NotLoginException.BE_REPLACED:
                resEnum = SaTokenExceptionEnum.REPLACED_LOGIN;
                break;
            case NotLoginException.KICK_OUT:
                resEnum = SaTokenExceptionEnum.KICK_OUT;
                break;
            default:
                resEnum = SaTokenExceptionEnum.UN_LOGIN;
                break;
        }
        return JSON.toJSONString(RES.no(resEnum));
    }
}