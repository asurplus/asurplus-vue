package com.asurplus.common.config;

import cn.dev33.satoken.exception.*;
import com.asurplus.common.enums.SaTokenExceptionEnum;
import com.asurplus.common.exception.CustomException;
import com.asurplus.common.utils.RES;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 全局异常处理
 *
 * @Author Lizhou
 */
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 自定义异常
     */
    @ExceptionHandler(CustomException.class)
    public RES customException(CustomException e) {
        return RES.no(e.getCode(), e.getMessage());
    }

    /**
     * 404异常
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public RES handlerNoFoundException(NoHandlerFoundException e) {
        return RES.no(404, "路径不存在，请检查路径是否正确");
    }

    /**
     * 字段验证异常
     */
    @ExceptionHandler(BindException.class)
    public RES bindException(BindException e) {
        return RES.no(e.getAllErrors().get(0).getDefaultMessage());
    }

    /**
     * 参数验证异常
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RES methodArgumentNotValidException(MethodArgumentNotValidException e) {
        return RES.no(e.getBindingResult().getFieldError().getDefaultMessage());
    }

    /**
     * 系统异常
     */
    @ExceptionHandler(Exception.class)
    public RES exception(Exception e) {
        // 无角色
        if (e instanceof NotRoleException) {
            NotRoleException nre = (NotRoleException) e;
            return RES.no(SaTokenExceptionEnum.NO_ROLE.getCode(), String.format(SaTokenExceptionEnum.NO_ROLE.getMsg(), nre.getRole()));
        }
        // 无权限
        if (e instanceof NotPermissionException) {
            NotPermissionException npe = (NotPermissionException) e;
            return RES.no(SaTokenExceptionEnum.NO_PERMISSION.getCode(), String.format(SaTokenExceptionEnum.NO_PERMISSION.getMsg(), npe.getPermission()));
        }
        // 账号封禁
        if (e instanceof DisableServiceException) {
            DisableServiceException dle = (DisableServiceException) e;
            return RES.no(SaTokenExceptionEnum.DISABLE_LOGIN.getCode(), String.format(SaTokenExceptionEnum.DISABLE_LOGIN.getMsg(), (-1 == dle.getDisableTime() ? "永久封禁" : dle.getDisableTime() + "秒后解封")));
        }
        // 二级认证
        if (e instanceof NotSafeException) {
            return RES.no(SaTokenExceptionEnum.NOT_SAFE);
        }
        // 路由匹配异常
        if (e instanceof StopMatchException) {
            return RES.no(SaTokenExceptionEnum.STOP_MATCH);
        }
        // 停止匹配
        if (e instanceof BackResultException) {
            BackResultException bre = (BackResultException) e;
            return RES.no(SaTokenExceptionEnum.NO_ROLE.getCode(), String.format(SaTokenExceptionEnum.NO_ROLE.getMsg(), bre.result));
        }
        return RES.no("操作失败：" + e.getMessage());
    }
}
