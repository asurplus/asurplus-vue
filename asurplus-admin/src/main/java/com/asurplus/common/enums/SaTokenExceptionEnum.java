package com.asurplus.common.enums;

import lombok.AllArgsConstructor;

/**
 * sa-token异常类
 *
 * @author asurplus
 */
@AllArgsConstructor
public enum SaTokenExceptionEnum implements BaseEnums {

    /**
     * 未提供身份认证信息
     */
    NO_TOKEN(401, "未提供身份认证信息"),
    /**
     * 未提供有效的身份认证信息
     */
    ERROR_TOKEN(401, "未提供有效的身份认证信息"),
    /**
     * 身份认证信息已过期，请重新登录
     */
    EXPIRE_TOKEN(401, "身份认证信息已过期，请重新登录"),
    /**
     * 账号已在另一台设备上登录，如非本人操作，请立即修改密码
     */
    REPLACED_LOGIN(401, "账号已在另一台设备上登录，如非本人操作，请立即修改密码"),
    /**
     * 账号已被系统强制下线
     */
    KICK_OUT(401, "账号已被系统强制下线"),
    /**
     * 账号未登录，请先登录
     */
    UN_LOGIN(401, "账号未登录，请先登录"),
    /**
     * 无此角色
     */
    NO_ROLE(403, "账号无此角色：%s"),
    /**
     * 无此权限
     */
    NO_PERMISSION(403, "账号无此权限：%s"),
    /**
     * 账号已被封禁
     */
    DISABLE_LOGIN(401, "账号已被封禁：%s"),
    /**
     * 二级认证失败
     */
    NOT_SAFE(401, "账号二级认证失败"),
    /**
     * 路由停止匹配
     */
    STOP_MATCH(500, "路由停止匹配"),
    /**
     * 停止匹配
     */
    BACK_RESULT(500, "停止匹配，输出结果：%s"),
    ;

    /**
     * 返回状态码
     */
    private final Integer code;

    /**
     * 返回消息
     */
    private final String msg;

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
